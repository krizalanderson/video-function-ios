//
//  LBVideoProcessor.h
//  LensbriksOpencvApp
//
//  Created by Kumar Vishal on 4/30/16.
//  Copyright © 2016 Sergey Yuzepovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

typedef struct {
    double location;
    double duration;
} OCVideoRange;

static inline OCVideoRange OCVideoRangeMake(double location, double duration)
{
    OCVideoRange vidRange_;
    vidRange_.location = location;
    vidRange_.duration = duration;
    return vidRange_;
}


@interface LBVideoProcessor : NSObject

- (id)initWithVideo:(NSURL *)vidURL;
- (AVAssetExportSession *)splitVideo:(OCVideoRange) range ToFile:(NSURL *)outputURL
                   completionHandler:(void (^)(NSURL *outputURL, NSError *error)) handler;


@end
