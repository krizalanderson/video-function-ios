//
//  AppDelegate.h
//  videoFunction
//
//  Created by Hupp Technologies on 30/08/16.
//  Copyright © 2016 Hupp Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
- (void)removeVideo:(NSString *)filename;

@end

