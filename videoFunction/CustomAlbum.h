//
//  CustomAlbum.h
//  CustomPhotoAlbum
//
//  Created by Balaji Malliswamy on 26/10/15.
//  Copyright © 2015 CodeSkip. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AVKit/AVKit.h"
@import Photos;

@interface CustomAlbum : NSObject

//Creating album with given name
+(void)makeAlbumWithTitle:(NSString *)title onSuccess:(void(^)(NSString *AlbumId))onSuccess onError: (void(^)(NSError * error)) onError;

//Get the album by name
+(PHAssetCollection *)getMyAlbumWithName:(NSString*)AlbumName;

//Add a Video
+(void)addNewAssetWithVideo:(NSURL *)Video toAlbum:(PHAssetCollection *)album onSuccess:(void(^)(NSString *VideoId))onSuccess onError: (void(^)(NSError * error)) onError;

//get the Video using identifier
+ (void)getVideoWithIdentifier:(NSString*)VideoId onSuccess:(void(^)(AVAsset *Video))onSuccess onError: (void(^)(NSError * error)) onError;

+(NSArray *)getAssets:(PHFetchResult *)fetch;

@end
