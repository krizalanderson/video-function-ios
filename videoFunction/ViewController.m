//
//  ViewController.m
//  videoFunction
//
//  Created by Hupp Technologies on 30/08/16.
//  Copyright © 2016 Hupp Technologies. All rights reserved.
//

#import "ViewController.h"
#import "AVKit/AVKit.h"
#import "CustomAlbum.h"
@import MediaPlayer;
@import Photos;

NSString * const CSAlbum = @"VideoFunction";
@interface ViewController (){
    
NSMutableArray *data;
NSMutableArray *temp;
UIImage *img;
__block AVAsset *url;
__block NSMutableArray *videoUrls;
NSString *albumId;
NSURL *docurl;
NSUInteger j;

NSMutableArray *urlOfDir;
}
@property (nonatomic, assign)BOOL overwrite;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    data = [[NSMutableArray alloc] init];  // store videos local identifier
    temp = [[NSMutableArray alloc] init];   // fetch data for comparing videos
    videoUrls = [[NSMutableArray alloc] init];  // store path of videos store locally
    urlOfDir = [[NSMutableArray alloc] init];   // store path of  stored videos at document directory
    [self createAlbum]; // create video album in iphone;
     self.overwrite = YES;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_group_t group = dispatch_group_create();
    
    // Add a task to the group
    dispatch_group_async(group, queue, ^{
        
        [self fetchAllVideo]; //// fetch all videos from video album
        NSLog(@"total video %lu",(unsigned long)[videoUrls count]);
    });
    
    dispatch_group_notify(group, queue, ^{
        j=0;
        [self saveVideo];
        
    });

}

-(void)fetchAllVideo  // fetch all videos from video album
{
    
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    PHFetchResult *fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeVideo options:fetchOptions];
    if (fetchResult != nil && fetchResult.count > 0)
    {
         for (PHAsset *asset1 in fetchResult)
         {
             [data addObject:asset1.localIdentifier];  //
             NSLog(@"data %@",data);
             NSLog(@"assest data %@",asset1);
             NSLog(@"video file name %@",[[fetchResult firstObject] filename]);
           
        }
    }
    int x=0;
    for (PHAsset *chkvideo in data)
    {
        if (x<=data.count)
        {
                      [CustomAlbum getVideoWithIdentifier:[NSString stringWithFormat:@"%@",[data objectAtIndex:x]] onSuccess:^(AVAsset *Video)
                       {
                           url = Video;
                           [videoUrls addObject:url];
                           NSLog(@"videos  %@",Video);
                           //[self saveVideo];
            
                       }onError:^(NSError *error)
                       {
                           NSLog(@"there is error");
                       }];
        x++;
        }
   }
    
}

- (void)saveVideo // save video to video to document directory after croping;
{
    //url = [videoUrls objectAtIndex:i];
    for (; j<videoUrls.count;j++)
    {
        url = [videoUrls objectAtIndex:j];
        NSURL *url2 = (NSURL *)[[(AVURLAsset *)url URL] fileReferenceURL];
        NSLog(@"url = %@", [url2 absoluteString]);
        NSLog(@"url = %@", [url2 relativePath]);
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *tempPath = [documentsDirectory stringByAppendingPathComponent:@"/m4v"];
        tempPath = [tempPath stringByAppendingString:[NSString stringWithFormat:@"%lu",(unsigned long)j]];
        tempPath = [tempPath stringByAppendingPathExtension:@"m4v"];
        NSLog(@"%@",tempPath);
        docurl = [NSURL fileURLWithPath:tempPath];
    
        LBVideoProcessor *obj = [[LBVideoProcessor alloc] initWithVideo:url2]; // crooping class object initialization
  
        OCVideoRange ocrange;
        ocrange.location=0;
        ocrange.duration=20.0;
    
    
        AVAssetExportSession *assestexportObj = [obj splitVideo:ocrange ToFile:docurl completionHandler:^(NSURL *outputURL, NSError *error)
        {
            NSLog(@"%@",[outputURL path]);
            NSLog(@"%@",error);
           [urlOfDir addObject:[outputURL lastPathComponent]];
        
            [CustomAlbum addNewAssetWithVideo:[NSURL URLWithString:[outputURL path]] toAlbum:[CustomAlbum getMyAlbumWithName:CSAlbum] onSuccess:^(NSString *VideoId)
             {
                 NSLog(@"%@",VideoId);
                 
             } onError:^(NSError *error)
             {
                 NSLog(@"probelm in saving image");
             }];
            
        }];
       NSLog(@"this is export assest session data %@",assestexportObj);
            
   }
                     
}

- (void)createAlbum
{
    [CustomAlbum makeAlbumWithTitle:CSAlbum onSuccess:^(NSString *AlbumId)
     {
         albumId = AlbumId;
         
     }onError:^(NSError *error)
     {
         NSLog(@"probelm in creating album");
     }];
    
    NSLog(@"album path %@",albumId);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
