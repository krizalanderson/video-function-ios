//
//  LBVideoProcessor.m
//  LensbriksOpencvApp
//
//  Created by Kumar Vishal on 4/30/16.
//  Copyright © 2016 Sergey Yuzepovich. All rights reserved.
//

#import "LBVideoProcessor.h"
#import <MediaPlayer/MediaPlayer.h>

@interface LBVideoProcessor()

@property (nonatomic, strong)AVURLAsset *sourceAsset;
@property (nonatomic, assign)BOOL overwrite;
@end

@implementation LBVideoProcessor

- (id) initWithVideo:(NSURL *)vidURL{
    if (self = [super init]){
        self.sourceAsset = [[AVURLAsset alloc] initWithURL:vidURL options:nil];
        if (!self.sourceAsset.isReadable)
        {
            self.sourceAsset = nil;
            return nil;
        }
        self.overwrite = YES;
    }
    return self;
}


- (AVAssetExportSession *)splitVideo:(OCVideoRange) range ToFile:(NSURL *)outputURL
                   completionHandler:(void (^)(NSURL *outputURL, NSError *error)) handler {
    AVAssetTrack *videoTrack = [[self.sourceAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    AVAssetTrack *audioTrack = [[self.sourceAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    
    self.overwrite = YES;
    if(!videoTrack || !audioTrack){
        handler(nil, [NSError errorWithDomain:@"com.occlusion.VideoProcessor"
                                         code:-2
                                     userInfo:@{NSLocalizedDescriptionKey:@"Input video does not contain any video and audio tracks."}]);
        return nil;
    }
    
    if([[NSFileManager defaultManager] fileExistsAtPath:outputURL.path]){
        if(self.overwrite){
            NSError *err = nil;
            if(![[NSFileManager defaultManager] removeItemAtURL:outputURL error:&err]){
                handler(nil, err);
                return nil;
            }
        }
        else{
            handler(nil, [NSError errorWithDomain:@"com.occlusion.VideoProcessor"
                                             code:-3
                                         userInfo:@{NSLocalizedDescriptionKey:@"The file already exists at outputURL."}]);
            return nil;
        }
    }
    
    double videoDuration = CMTimeGetSeconds(videoTrack.timeRange.duration);
    if(videoDuration < range.location){
        handler(nil, [NSError errorWithDomain:@"com.occlusion.VideoProcessor"
                                         code:-4
                                     userInfo:@{NSLocalizedDescriptionKey:@"The beginning time for split is over total video time."}]);
        return nil;
    }
    else if(videoDuration < (range.location+range.duration)){
        range.duration = videoDuration-range.location;
    }
    
    AVMutableComposition *composition = [AVMutableComposition composition];
    AVMutableCompositionTrack *compositionVideoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                                preferredTrackID:kCMPersistentTrackID_Invalid];
    
    AVMutableCompositionTrack *compositionAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio
                                                                                preferredTrackID:kCMPersistentTrackID_Invalid];

    CMTime begin = CMTimeMakeWithSeconds(range.location, videoTrack.timeRange.duration.timescale);
    CMTime duration = CMTimeMakeWithSeconds(range.duration, videoTrack.timeRange.duration.timescale);
    [compositionVideoTrack insertTimeRange:CMTimeRangeMake(begin, duration)
                                   ofTrack:videoTrack
                                    atTime:kCMTimeZero
                                     error:nil];
    
    
    begin = CMTimeMakeWithSeconds(range.location, audioTrack.timeRange.duration.timescale);
    duration = CMTimeMakeWithSeconds(range.duration, audioTrack.timeRange.duration.timescale);
    [compositionAudioTrack insertTimeRange:CMTimeRangeMake(begin, duration)
                                   ofTrack:audioTrack
                                    atTime:kCMTimeZero
                                     error:nil];
    
    
    compositionVideoTrack.preferredTransform = videoTrack.preferredTransform;
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:composition
                                                                           presetName:AVAssetExportPresetPassthrough];
    
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeAppleM4V;
    //exportSession.outputFileType = AVFileTypeMPEG4;
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        switch(exportSession.status){
            case AVAssetExportSessionStatusFailed:
                handler(nil, exportSession.error);
                // NSLog(@"Error :%@", exportSession.error);
                break;
            case AVAssetExportSessionStatusCancelled:
                handler(nil, exportSession.error);
                // NSLog(@"Cancelled");
                break;
            case AVAssetExportSessionStatusCompleted:
                handler(outputURL, exportSession.error);
                // NSLog(@"Completed");
                break;
            default:
                break;
        }
    }];
    return exportSession;
}

@end
