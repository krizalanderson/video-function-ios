//
//  ViewController.h
//  videoFunction
//
//  Created by Hupp Technologies on 30/08/16.
//  Copyright © 2016 Hupp Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LBVideoProcessor.h"


@interface ViewController : UIViewController

-(void)fetchAllVideo;  // fetch all videos from video album
- (void)saveVideo;  // save video to video to document directo after croping;

@end

